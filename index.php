<?php
require('animal.php');
require('Frog.php');
require('Ape.php');


echo "Release 0 <br><br>";
$sheep = new Animal("shaun");

echo "Nama Hewan :" . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki :" . $sheep->legs . "<br>"; // 2
echo "Berdarah Dingin :" . $sheep->cold_blooded . "<br><br>"; // false

echo "Release 1 <br><br>";
$sungokong = new Ape("kera sakti");
echo "Nama Hewan :" . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki :" . $sungokong->legs . "<br>"; // 2
echo "Berdarah Dingin :" . $sungokong->cold_blooded . "<br>"; // false 
$sungokong->yell(); // "Auooo"

echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan :" . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki :" . $kodok->legs . "<br>"; // 2
echo "Berdarah Dingin :" . $kodok->cold_blooded . "<br>"; // false
$kodok->jump(); // "hop hop"
